resource "aws_apigatewayv2_api" "pyperchan_apigateway_api" {
  name          = "pyPerChan"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_integration" "pyperchan_apigateway_integration" {
  api_id           = aws_apigatewayv2_api.pyperchan_apigateway_api.id
  integration_type = "AWS_PROXY"

  integration_method     = "POST"
  integration_uri        = aws_lambda_function.pyperchan_defer_lambda_function.arn
  payload_format_version = "2.0"
  timeout_milliseconds   = 3000
}

resource "aws_apigatewayv2_route" "pyperchan_apigateway_route" {
  api_id    = aws_apigatewayv2_api.pyperchan_apigateway_api.id
  route_key = "POST /"

  target = "integrations/${aws_apigatewayv2_integration.pyperchan_apigateway_integration.id}"
}
