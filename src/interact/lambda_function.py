def secrets_manager_client():
    import boto3

    return boto3.session.Session().client(
        service_name="secretsmanager", region_name="eu-west-2"
    )


def discord_error_response(application_id, token, discord_secret):
    import json
    import requests

    return requests.patch(
        url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
        data=json.dumps({"content": "An error occured!"}),
        headers={
            "Authorization": f"Bot {discord_secret}",
            "Content-Type": "application/json",
        },
    )


def bad_status_code(status_code, content):
    print({"statusCode": status_code, "content": content})
    exit(1)


def lambda_handler(event, context) -> None:
    command = event["data"]["name"]
    application_id = event["application_id"]
    token = event["token"]

    try:
        if command == "ping":
            from commands.ping import ping_command

            ping = ping_command(application_id, token)
            status_code = ping["statusCode"]
            content = ping["content"]
        elif command == "quote":
            from commands.quote import quote_command

            guild_id = event["guild_id"]
            user_id = event["data"]["options"][0]["options"][0]["value"]

            quote = quote_command(event, user_id, guild_id, application_id, token)
            status_code = quote["statusCode"]
            content = quote["content"]
        else:
            import json
            from azure.cosmos import CosmosClient

            secrets_manager = secrets_manager_client()
            cosmos_secret = json.loads(
                secrets_manager.get_secret_value(SecretId="pyPerChan_azure")[
                    "SecretString"
                ]
            )

            cosmos_client = (
                CosmosClient(
                    cosmos_secret["cosmos_url"],
                    cosmos_secret["cosmos_read"],
                    consistency_level="Eventual",
                )
                .get_database_client("pyperchan")
                .get_container_client("canned_responses")
            )

            commands = list()
            for i in cosmos_client.query_items(
                "SELECT canned_responses.command FROM canned_responses",
                enable_cross_partition_query=True,
            ):
                commands.append(i["command"])

            if command in commands:
                from commands.canned_response import canned_response_command

                guild_id = event["guild_id"]

                canned_response = canned_response_command(
                    command,
                    guild_id,
                    application_id,
                    token,
                    secrets_manager,
                    cosmos_client,
                )
                status_code = canned_response["statusCode"]
                content = canned_response["content"]
            else:
                print(f"Unknown command: {command}")

                error = discord_error_response(application_id, token, discord_secret)
                status_code = error.status_code
                content = error.content

        if status_code != 200:
            bad_status_code(status_code, content)

    except Exception as e:
        import json
        import requests

        print(e)

        try:
            discord_secret = json.loads(
                secrets_manager.get_secret_value(SecretId="pyPerChan_discord")[
                    "SecretString"
                ]
            )["discord_bot_token"]
        except NameError:
            secrets_manager = secrets_manager_client()
            discord_secret = json.loads(
                secrets_manager.get_secret_value(SecretId="pyPerChan_discord")[
                    "SecretString"
                ]
            )["discord_bot_token"]

        error = discord_error_response(application_id, token, discord_secret)
        status_code = error.status_code
        content = error.content

        if status_code != 200:
            bad_status_code(status_code, content)
