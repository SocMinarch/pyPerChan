resource "aws_iam_policy" "pyperchan_ec2_createnetworkinterface_iam_policy" {
  name = "pyPerChanEc2CreateNetworkInterface"

  policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow",
          Action = [
            "ec2:CreateNetworkInterface",
            "ec2:DescribeNetworkInterfaces",
            "ec2:DeleteNetworkInterface"
          ]
          Resource = "*"
        },
      ]
    }
  )
}

resource "aws_iam_role" "pyperchan_interact_iam_role" {
  name = "pyPerChanInteract"
  assume_role_policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Principal = {
            Service = "lambda.amazonaws.com"
          }
          Action = "sts:AssumeRole"
        }
      ]
    }
  )

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    aws_iam_policy.pyperchan_secretsmanager_iam_policy.arn,
    aws_iam_policy.pyperchan_ec2_createnetworkinterface_iam_policy.arn
  ]
  path = "/service-role/"
}

data "archive_file" "pyperchan_interact_archive_file" {
  type        = "zip"
  source_dir  = "${path.module}/../src/interact"
  output_path = "interact.zip"
}

resource "aws_cloudwatch_log_group" "pyperchan_interact_log_group" {
  name              = "/aws/lambda/pyPerChan_interact"
  retention_in_days = 7
}

data "aws_s3_object" "pyperchan_interact_lambda_layer" {
  bucket = "pyperchan"
  key    = "interact_layer.zip"
}

resource "aws_lambda_layer_version" "pyperchan_interact_lambda_layer" {
  layer_name = "interact_layer"

  compatible_architectures = [var.architectures]
  compatible_runtimes      = [var.runtime]
  s3_bucket                = "pyperchan"
  s3_key                   = "interact_layer.zip"
  source_code_hash         = data.aws_s3_object.pyperchan_interact_lambda_layer.metadata.Sha256
}

resource "aws_lambda_function" "pyperchan_interact_lambda_function" {
  function_name = "pyPerChan_interact"
  role          = aws_iam_role.pyperchan_interact_iam_role.arn

  architectures = [var.architectures]

  filename         = data.archive_file.pyperchan_interact_archive_file.output_path
  layers           = [aws_lambda_layer_version.pyperchan_interact_lambda_layer.arn]
  memory_size      = 128
  runtime          = var.runtime
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256(data.archive_file.pyperchan_interact_archive_file.output_path)
  timeout          = 60

  vpc_config {
    security_group_ids = [aws_default_security_group.pyperchan_vpc_default_security_group.id]
    subnet_ids         = [aws_subnet.pyperchan_lambda_subnet.id]
  }

  depends_on = [
    aws_cloudwatch_log_group.pyperchan_interact_log_group
  ]
}
