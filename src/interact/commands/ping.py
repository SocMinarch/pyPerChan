import boto3
import json
import requests


def ping_command(application_id, token) -> dict:
    discord_secret = json.loads(
        boto3.session.Session()
        .client(service_name="secretsmanager", region_name="eu-west-2")
        .get_secret_value(SecretId="pyPerChan_discord")["SecretString"]
    )["discord_bot_token"]

    ping = requests.patch(
        url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
        data=json.dumps({"content": "Pong"}),
        headers={
            "Authorization": f"Bot {discord_secret}",
            "Content-Type": "application/json",
        },
    )

    return {"statusCode": ping.status_code, "content": json.loads(ping.content)}
