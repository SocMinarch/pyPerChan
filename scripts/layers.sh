#!/bin/bash -e

DIR=$(readlink -f "$(dirname "$0")")
LAYERS="$DIR/../layers"

rm -rf "$LAYERS"

layer() {
    mkdir -p "$LAYERS"/"$1"/python

    if [ -f "$LAYERS"/"$1"/"$1"_layer.zip ]
    then
        rm "$LAYERS"/"$1"/"$1"_layer.zip
    fi

    docker run --rm --pull always --platform linux/arm64 --mount type=bind,src="$(readlink -f "$DIR"/../layers/"$1"/python)",target=/layers,readonly=false --mount type=bind,src="$(readlink -f "$DIR"/../src/"$1"/requirements.txt)",target=/requirements.txt,readonly=true python:3.9-slim pip install -t layers/ $PACKAGES
    docker run --rm --pull always --platform linux/arm64 --mount type=bind,src="$(readlink -f "$DIR"/../layers/"$1"/python)",target=/layers,readonly=false --mount type=bind,src="$(readlink -f "$DIR"/../src/"$1"/requirements.txt)",target=/requirements.txt,readonly=false python:3.9-slim sh -c "pip freeze --path layers/ > /requirements.txt"
    PWD=$(pwd)
    cd "$DIR"/../layers/"$1" || exit 1
    zip -r9 "$1"_layer python
    cd "$PWD" || exit 1
}

for i in defer interact
do
    if [ $i == "defer" ]
    then
        PACKAGES="discord-interactions==0.4.0"
    elif [ $i == "interact" ]
    then
        PACKAGES="requests==2.28.1 azure-cosmos==4.3.0"
    fi
    layer $i
done
