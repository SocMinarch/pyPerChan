resource "aws_iam_policy" "pyperchan_secretsmanager_iam_policy" {
  name = "pyPerChanSecretsManager"

  policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Effect   = "Allow"
          Action   = "secretsmanager:GetSecretValue",
          Resource = "*"
        },
      ]
    }
  )
}

resource "aws_iam_role" "pyperchan_defer_iam_role" {
  name = "pyPerChanDefer"
  assume_role_policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Principal = {
            Service = "lambda.amazonaws.com"
          }
          Action = "sts:AssumeRole"
        }
      ]
    }
  )

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    aws_iam_policy.pyperchan_secretsmanager_iam_policy.arn
  ]
  path = "/service-role/"
}

resource "aws_secretsmanager_secret" "pyperchan_discord_secretsmanager_secret" {
  name = "pyPerChan_discord"
}

resource "aws_secretsmanager_secret_version" "pyperchan_discord_secretsmanager_secret_version" {
  secret_id = aws_secretsmanager_secret.pyperchan_discord_secretsmanager_secret.arn
  secret_string = jsonencode(
    {
      discord_public_key = var.discord_public_key
      discord_bot_token  = var.discord_bot_token
    }
  )
}

data "archive_file" "pyperchan_defer_archive_file" {
  type        = "zip"
  source_file = "${path.module}/../src/defer/lambda_function.py"
  output_path = "defer.zip"
}

resource "aws_cloudwatch_log_group" "pyperchan_defer_log_group" {
  name              = "/aws/lambda/pyPerChan_defer"
  retention_in_days = 7
}

data "aws_s3_object" "pyperchan_defer_lambda_layer" {
  bucket = "pyperchan"
  key    = "defer_layer.zip"
}

resource "aws_lambda_layer_version" "pyperchan_defer_lambda_layer" {
  layer_name = "defer_layer"

  compatible_architectures = [var.architectures]
  compatible_runtimes      = [var.runtime]
  s3_bucket                = "pyperchan"
  s3_key                   = "defer_layer.zip"
  source_code_hash         = data.aws_s3_object.pyperchan_defer_lambda_layer.metadata.Sha256
}

resource "aws_lambda_function" "pyperchan_defer_lambda_function" {
  function_name = "pyPerChan_defer"
  role          = aws_iam_role.pyperchan_defer_iam_role.arn

  architectures = [var.architectures]

  environment {
    variables = {
      DISCORD_PUBLIC_KEY = var.discord_public_key
    }
  }

  filename         = data.archive_file.pyperchan_defer_archive_file.output_path
  layers           = [aws_lambda_layer_version.pyperchan_defer_lambda_layer.arn]
  memory_size      = 128
  runtime          = var.runtime
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256(data.archive_file.pyperchan_defer_archive_file.output_path)

  depends_on = [
    aws_cloudwatch_log_group.pyperchan_defer_log_group
  ]
}

resource "aws_lambda_permission" "pyperchan_defer_lambda_permission" {
  statement_id  = "pyPerChanDefer"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.pyperchan_defer_lambda_function.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.pyperchan_apigateway_api.execution_arn}/*/*/"
}
