import json
import requests


def canned_response_command(
    command, guild_id, application_id, token, secrets_manager, cosmos_client
) -> dict:
    responses = list()
    discord_secret = json.loads(
        secrets_manager.get_secret_value(SecretId="pyPerChan_discord")["SecretString"]
    )["discord_bot_token"]

    try:
        responses = list()

        for i in cosmos_client.query_items(
            f"SELECT canned_responses.response FROM canned_responses WHERE canned_responses.command = '{command}' AND canned_responses.guild_id = '{guild_id}'",
            enable_cross_partition_query=True,
        ):
            responses.append(i["response"])
        if len(responses) == 0:
            response = responses[0]
        else:
            import random

            response = random.choice(responses)

    except Exception as e:
        print(e)
        error = requests.patch(
            url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
            data=json.dumps(
                {"content": "Command does not have any responses defined!"}
            ),
            headers={
                "Authorization": f"Bot {discord_secret}",
                "Content-Type": "application/json",
            },
        )
        exit(1)

    canned_response = requests.patch(
        url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
        data=json.dumps({"content": f"{response}"}),
        headers={
            "Authorization": f"Bot {discord_secret}",
            "Content-Type": "application/json",
        },
    )

    return {
        "statusCode": canned_response.status_code,
        "content": json.loads(canned_response.content),
    }
