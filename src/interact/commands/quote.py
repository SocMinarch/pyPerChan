import boto3
import json
from azure.cosmos import CosmosClient
import requests

secrets_manager = boto3.session.Session().client(
    service_name="secretsmanager", region_name="eu-west-2"
)

cosmos_secret = json.loads(
    secrets_manager.get_secret_value(SecretId="pyPerChan_azure")["SecretString"]
)


def get_quote(user_id, guild_id, application_id, token) -> dict:
    import random

    quotes = list()
    for i in (
        CosmosClient(
            cosmos_secret["cosmos_url"],
            cosmos_secret["cosmos_read"],
            consistency_level="Eventual",
        )
        .get_database_client("pyperchan")
        .get_container_client("quotes")
        .query_items(
            f"SELECT quotes.message, quotes.user_id FROM quotes WHERE quotes.user_id = '{user_id}' AND quotes.guild_id = '{guild_id}'",
            enable_cross_partition_query=True,
        )
    ):
        quotes.append(i)
    quote = random.choice(quotes)

    discord_secret = json.loads(
        secrets_manager.get_secret_value(SecretId="pyPerChan_discord")["SecretString"]
    )["discord_bot_token"]

    quote = requests.patch(
        url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
        data=json.dumps({"content": f"> {quote['message']}\n- <@{quote['user_id']}>"}),
        headers={
            "Authorization": f"Bot {discord_secret}",
            "Content-Type": "application/json",
        },
    )

    return {"statusCode": quote.status_code, "content": json.loads(quote.content)}


def add_quote(message, user_id, guild_id, application_id, token) -> dict:
    CosmosClient(
        cosmos_secret["cosmos_url"],
        cosmos_secret["cosmos_write"],
        consistency_level="Strong",
    ).get_database_client("pyperchan").get_container_client("quotes").create_item(
        {"message": message, "user_id": user_id, "guild_id": guild_id},
        enable_automatic_id_generation=True,
    )

    discord_secret = json.loads(
        secrets_manager.get_secret_value(SecretId="pyPerChan_discord")["SecretString"]
    )["discord_bot_token"]

    quote = requests.patch(
        url=f"https://discordapp.com/api/webhooks/{application_id}/{token}/messages/@original",
        data=json.dumps({"content": f"> {message}\n- <@{user_id}>"}),
        headers={
            "Authorization": f"Bot {discord_secret}",
            "Content-Type": "application/json",
        },
    )

    return {"statusCode": quote.status_code, "content": json.loads(quote.content)}


def quote_command(event, user_id, guild_id, application_id, token) -> dict:
    command = event["data"]["options"][0]["name"]

    if command == "get":
        quote = get_quote(user_id, guild_id, application_id, token)
    elif command == "add":
        quote = add_quote(
            event["data"]["options"][0]["options"][1]["value"],
            user_id,
            guild_id,
            application_id,
            token,
        )

    return quote
