resource "azurerm_cosmosdb_sql_container" "canned_responses_cosmosdb_sql_container" {
  name                = "canned_responses"
  resource_group_name = azurerm_resource_group.pyperchan_resource_group.name
  account_name        = azurerm_cosmosdb_account.pyperchan_cosmosdb_account.name
  database_name       = azurerm_cosmosdb_sql_database.pyperchan_cosmosdb_sql_database.name
  partition_key_path  = "/response"

  partition_key_version = 2

  indexing_policy {
    indexing_mode = "consistent"

    excluded_path {
      path = "/*"
    }

    composite_index {
      index {
        path  = "/command"
        order = "Ascending"
      }

      index {
        path  = "/guild_id"
        order = "Ascending"
      }
    }
  }

  unique_key {
    paths = ["/command", "/response", "/guild_id"]
  }
}
