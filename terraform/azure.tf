resource "aws_secretsmanager_secret" "azure_secretsmanager_secret" {
  name = "pyPerChan_azure"
}

resource "aws_secretsmanager_secret_version" "azure_secretsmanager_secret_version" {
  secret_id = aws_secretsmanager_secret.azure_secretsmanager_secret.arn
  secret_string = jsonencode(
    {
      cosmos_url   = var.azure_cosmosdb_url
      cosmos_read  = var.azure_cosmosdb_read
      cosmos_write = var.azure_cosmosdb_write
    }
  )
}

resource "azurerm_resource_group" "pyperchan_resource_group" {
  name     = "pyPerChan"
  location = "UK South"
}

resource "azurerm_cosmosdb_account" "pyperchan_cosmosdb_account" {
  name                = "pyperchan"
  resource_group_name = azurerm_resource_group.pyperchan_resource_group.name
  location            = azurerm_resource_group.pyperchan_resource_group.location
  offer_type          = "Standard"

  consistency_policy {
    consistency_level = "Strong"
  }

  ip_range_filter = "${aws_eip.pyperchan_elastic_ip.public_ip},${var.management_ip}"

  geo_location {
    location          = "ukwest"
    failover_priority = 0
    zone_redundant    = false
  }

  enable_free_tier = true
}

resource "azurerm_cosmosdb_sql_database" "pyperchan_cosmosdb_sql_database" {
  name                = "pyperchan"
  resource_group_name = azurerm_resource_group.pyperchan_resource_group.name
  account_name        = azurerm_cosmosdb_account.pyperchan_cosmosdb_account.name

  throughput = 500
}
