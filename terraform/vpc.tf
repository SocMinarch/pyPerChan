resource "aws_vpc" "pyperchan_vpc" {
  cidr_block = "192.168.0.0/27"

  tags = {
    Name = "pyPerChan"
  }
}

resource "aws_subnet" "pyperchan_lambda_subnet" {
  vpc_id     = aws_vpc.pyperchan_vpc.id
  cidr_block = "192.168.0.0/28"

  tags = {
    Name = "lambda"
  }
}

resource "aws_subnet" "pyperchan_nat_subnet" {
  vpc_id     = aws_vpc.pyperchan_vpc.id
  cidr_block = "192.168.0.16/28"

  tags = {
    Name = "nat"
  }
}

resource "aws_internet_gateway" "pyperchan_internet_gateway" {
  vpc_id = aws_vpc.pyperchan_vpc.id

  tags = {
    Name = "pyPerChan"
  }
}

resource "aws_eip" "pyperchan_elastic_ip" {
  vpc = true

  tags = {
    Name = "lambda"
  }
}

resource "aws_nat_gateway" "pyperchan_nat_gateway" {
  allocation_id = aws_eip.pyperchan_elastic_ip.id
  subnet_id     = aws_subnet.pyperchan_nat_subnet.id

  tags = {
    Name = "pyPerChan"
  }

  depends_on = [aws_internet_gateway.pyperchan_internet_gateway]
}

resource "aws_default_security_group" "pyperchan_vpc_default_security_group" {
  vpc_id = aws_vpc.pyperchan_vpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_default_route_table" "pyperchan_lambda_route_table" {
  default_route_table_id = aws_vpc.pyperchan_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"

    nat_gateway_id = aws_nat_gateway.pyperchan_nat_gateway.id
  }

  tags = {
    Name = "lambda"
  }
}

resource "aws_route_table" "pyperchan_nat_route_table" {
  vpc_id = aws_vpc.pyperchan_vpc.id

  route {
    cidr_block = "0.0.0.0/0"

    gateway_id = aws_internet_gateway.pyperchan_internet_gateway.id
  }

  tags = {
    Name = "nat"
  }
}

resource "aws_route_table_association" "pyperchan_nat_route_table_association" {
  subnet_id      = aws_subnet.pyperchan_nat_subnet.id
  route_table_id = aws_route_table.pyperchan_nat_route_table.id
}
