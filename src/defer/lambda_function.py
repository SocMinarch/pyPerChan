import boto3
import json
from discord_interactions import verify_key, InteractionType, InteractionResponseType


def lambda_handler(event, context):
    headers = event["headers"]
    raw_body = event["body"].encode()

    if not verify_key(
        raw_body,
        headers["x-signature-ed25519"],
        headers["x-signature-timestamp"],
        json.loads(
            boto3.session.Session()
            .client(service_name="secretsmanager", region_name="eu-west-2")
            .get_secret_value(SecretId="pyPerChan_discord")["SecretString"]
        )["discord_public_key"],
    ):
        return {"statusCode": 401}

    body = json.loads(raw_body)

    if body["type"] == InteractionType.APPLICATION_COMMAND:
        boto3.client("lambda").invoke(
            FunctionName="pyPerChan_interact",
            InvocationType="Event",
            Payload=raw_body,
        )

        return {"type": InteractionResponseType.DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE}
    elif body["type"] == InteractionType.PING:
        return {"type": InteractionResponseType.PONG}
