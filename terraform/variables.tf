variable "aws_access_key" {
  type      = string
  sensitive = true
}

variable "aws_secret_key" {
  type      = string
  sensitive = true
}

variable "aws_region" {
  type = string
}

variable "runtime" {
  type    = string
  default = "python3.9"
}

variable "architectures" {
  type    = string
  default = "arm64"
}

variable "discord_public_key" {
  type      = string
  sensitive = true
}

variable "discord_bot_token" {
  type      = string
  sensitive = true
}

variable "azure_subscription_id" {
  type      = string
  sensitive = true
}

variable "azure_app_id" {
  type      = string
  sensitive = true
}

variable "azure_password" {
  type      = string
  sensitive = true
}

variable "azure_tenant" {
  type      = string
  sensitive = true
}

variable "azure_cosmosdb_url" {
  type      = string
  sensitive = true
}

variable "azure_cosmosdb_read" {
  type      = string
  sensitive = true
}

variable "azure_cosmosdb_write" {
  type      = string
  sensitive = true
}

variable "management_ip" {
  type      = string
  sensitive = true
}
