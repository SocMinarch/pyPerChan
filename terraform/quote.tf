resource "azurerm_cosmosdb_sql_container" "quotes_cosmosdb_sql_container" {
  name                = "quotes"
  resource_group_name = azurerm_resource_group.pyperchan_resource_group.name
  account_name        = azurerm_cosmosdb_account.pyperchan_cosmosdb_account.name
  database_name       = azurerm_cosmosdb_sql_database.pyperchan_cosmosdb_sql_database.name
  partition_key_path  = "/message"

  partition_key_version = 2

  indexing_policy {
    indexing_mode = "consistent"

    excluded_path {
      path = "/*"
    }

    composite_index {
      index {
        path  = "/user_id"
        order = "Ascending"
      }

      index {
        path  = "/guild_id"
        order = "Ascending"
      }
    }
  }

  unique_key {
    paths = ["/message", "/user_id", "/guild_id"]
  }
}
